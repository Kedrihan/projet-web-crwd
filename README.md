# README #

# Lancer les tests #
```
makefile.bat tests
```

# Installer la base de données #
définir la base de données dans le fichier .env (aide dans le fichier .env.dist)
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:mig
```

# Installer l'envoi de mails #
ajouter les lignes de configuration dans le fichier .env (aide dans le fichier .env.dist)

# Les Commandes du projet #
* Installer le projet: makefile.bat init
* Mettre à jour les vendors : makefile.bat update
* vider le cache : makefile.bat cc
* installer les assets : makefile.bat assets
* ajouter un bundle : bin/console composer req [nom_du_bundle]
* supprimer un bundle : bin/console composer remove [nom_du_bundle]

# Les Commandes GIT #
* récupérer les modifications du serveur : git pull
* ajouter un fichier à un commit : git add [nom_du_fichier]
* commiter des changements : git commit -m "[description des changements]"
* envoyer des changements sur le serveur : git push
* créer une branche : git checkout -b [nom_de_la_branche]
* changer de branche : git checkout [nom_de_la_branche]

# Corriger une Issue #
Ajouter dans le commentaire du commit la reference de l'issue ex :
```
git commit -m "fix #2"
```

# Si il y a des erreurs après l'installation #
* vérifier si la base de données est créé et complète (avec toutes les tables)
* vérifier si il y a la connexion à la base de données et la configuration de l'envoi de mail dans le fichier .env
* vérifier si les vendors ont été installé/mis à jour