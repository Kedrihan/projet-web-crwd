@ECHO off

SETLOCAL EnableDelayedExpansion

FOR /f "delims=" %%x IN (.env) DO (
    SET "VAR=%%x"
    IF NOT "!VAR:~0,1!" == "#" (
        ECHO setting %%x
        SET "%%x"
    )
)

SET CONSOLE=bin\console

:sf_console
    IF NOT EXIST %CONSOLE% (
        ECHO Run "composer require cli" to install the Symfony console.
    )

    IF NOT "%1" == "" (
        call :%1
    )

    EXIT /B %ERRORLEVEL%

:cc
    IF NOT EXIST %CONSOLE% (
        SET FOLDER=var\cache
        IF EXIST "%FOLDER%" (
            FOR /F %%x IN ('dir %FOLDER% /B') DO (
                RMDIR /S /Q "%FOLDER%\%%x"
            )
        )
    ) ELSE (
        php %CONSOLE% cache:clear --no-warmup
    )

    EXIT /B %ERRORLEVEL%


:serve_as_sf
    IF NOT EXIST %CONSOLE% (
        call :serve_as_php
    ) ELSE (
        FOR /F %%x IN ('php %CONSOLE% ^| FIND /I "server:start"') DO (
            SET FOUND=%%x
        )

        IF NOT "!FOUND!" == "" (
            php %CONSOLE% server:start /q
            echo Quit the server with bin/console server:stop
        ) ELSE (
            call :serve_as_php
        )
    )

    EXIT /B %ERRORLEVEL%

:serve_as_php
	php -S 127.0.0.1:80 -t public

    EXIT /B %ERRORLEVEL%

:up
	call :serve_as_php

    EXIT /B %ERRORLEVEL%

:assets
	php %CONSOLE% assets:install
    
    EXIT /B %ERRORLEVEL%

:init
	php composer.phar self-update
	php composer.phar install -o
	call :cc
	call :assets
    EXIT /B %ERRORLEVEL%

:update
	php composer.phar self-update
	php composer.phar update -o
    EXIT /B %ERRORLEVEL%

:tests
    php vendor/atoum/atoum/bin/atoum