var $collectionHolder;

// setup an "add a photo" link
var $addphotoLink = $('<a href="#" class="add_photo_link">Ajouter une photo</a>');
var $adddisponibilityLink = $('<a href="#" class="add_disponibilitie_link">Ajouter une date</a>');

var $newLinkLi = $('<li></li>').append($addphotoLink);
var $newLinkLidispo = $('<li></li>').append($adddisponibilityLink);

function addphotoForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your photos field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a photo" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);


    addPhotoFormDeleteLink($newFormLi);
}
function addPhotoFormDeleteLink($tagFormLi) {

    if (typeof $tagFormLi.find('input')[0] === 'object' ) {
        var $removeFormA = $('<a href="#">Supprimer ce champ</a>');
        $tagFormLi.append($removeFormA);

        $removeFormA.on('click', function (e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the tag form
            $tagFormLi.remove();
        });
    }
}

jQuery(document).ready(function() {
    // Get the ul that holds the collection of photos
    $collectionHolder = $('ul.photos');
    $collectionHolderDispo = $('ul.disponibilities');

    // add a delete link to all of the existing tag form li elements
    $collectionHolder.find('li').each(function() {
        addPhotoFormDeleteLink($(this));
    });
    // add a delete link to all of the existing tag form li elements
    $collectionHolderDispo.find('li').each(function() {
        addPhotoFormDeleteLink($(this));
    });

    // add the "add a photo" anchor and li to the photos ul
    $collectionHolder.append($newLinkLi);
    $collectionHolderDispo.append($newLinkLidispo);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $collectionHolderDispo.data('index2', $collectionHolderDispo.find(':input').length);

    $addphotoLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new photo form (see next code block)
        addphotoForm($collectionHolder, $newLinkLi);
    });

    $adddisponibilityLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new photo form (see next code block)
        addphotoForm($collectionHolderDispo, $newLinkLidispo);
    });
});