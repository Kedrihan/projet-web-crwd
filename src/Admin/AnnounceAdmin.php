<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class AnnounceAdmin extends AbstractAdmin
{
    private $oldAnnounce;

    // Fields to be shown on create/edit forms

    public function preUpdate($announce)
    {
        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $this->oldAnnounce = $em->getUnitOfWork()->getOriginalEntityData($announce);
    }

    public function postUpdate($announce)
    {
        if ($this->oldAnnounce['isActive'] !== $announce->getIsActive()) {
            if ($announce->getIsActive() === true) {
                $transport = new Swift_SmtpTransport('smtp.gmail.com', 465, "SSL");
                $transport->setUsername('zikaloka.contact')
                    ->setPassword('coucousalutmdrptdrlol1!');

                $mailer = new \Swift_Mailer($transport);

                $message = new Swift_Message();
                $message->setFrom(array('zikaloka.contact@gmail.com' => 'Zikaloka'))
                    ->setSubject('Zikaloka : Annonce acceptée')
                    ->setTo(array($announce->getUser()->getEmail()))
                    ->setBody('Votre annonce est acceptée');

                $result = $mailer->send($message);
            } else if ($announce->getIsActive() === false) {
                $transport = new Swift_SmtpTransport('smtp.gmail.com', 465, "SSL");
                $transport->setUsername('zikaloka.contact')
                    ->setPassword('coucousalutmdrptdrlol1!');

                $mailer = new Swift_Mailer($transport);

                $message = new Swift_Message();
                $message->setFrom(array('zikaloka.contact@gmail.com' => 'Zikaloka'))
                    ->setSubject('Zikaloka : Annonce refusée')
                    ->setTo(array($announce->getUser()->getEmail()))
                    ->setBody('Votre annonce est refusée');

                $result = $mailer->send($message);
            }
        }
    }

    // Fields to be shown on filter forms

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    // Fields to be shown on lists

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('description')
            ->add('reference')
            ->add('brandname')
            ->add('purchase_date', DateType::class, array(
                'widget' => 'single_text',

            ))
            ->add('price')
            ->add('isActive');
    }

    // Fields to be shown on show action

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('reference')
            ->add('brandname')
            ->add('purchase_date')
            ->add('price')
            ->add('isActive');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('reference')
            ->addIdentifier('brandname')
            ->addIdentifier('purchase_date')
            ->addIdentifier('price')
            ->add('isActive');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // get the current Image instance
        $image = $this->getSubject();

        $showMapper
            ->add('title')
            ->add('reference')
            ->add('brandname')
            ->add('purchase_date')
            ->add('price')
            ->add('isActive')
            ->add('path', null, array('template' => 'Admin/list_image.html.twig'));
    }
}