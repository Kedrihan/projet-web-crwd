<?php

namespace App\Controller;

use App\Entity\Announce;
use App\Entity\Disponibility;
use App\Entity\Message;
use App\Entity\Photo;
use App\Entity\Rental;
use App\Form\AnnounceType;
use App\Form\DisponibilityType;
use App\Form\MessageType;
use App\Form\PhotoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class AnnounceController extends Controller
{
    //affiche la liste des announces de l'utilisateur connecté
    public function listAction()
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findBy(['user' => $this->getUser()]);

        return $this->render(
            'list_announce.html.twig',
            [
                'listAnnounce' => $announce,
            ]
        );
    }

    //permet a l'utilisateur de modifier l'annonce
    public function editAction(Request $request, $id)
    {

        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }

        $formAnnounce = $this->createForm(AnnounceType::class, $announce)
            ->add('save', SubmitType::class, ['label' => 'Modifier']);

        $formAnnounce->handleRequest($request);


        if ($formAnnounce->isSubmitted() && $formAnnounce->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($announce);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('announce_part2', array('id' => $announce->getId())));
        }

        return $this->render(
            'Announce/add_announce.html.twig',
            [
                'formAnnounce' => $formAnnounce->createView(),
            ]
        );
    }

    //permet d'activer ou de désactiver une annonce
    public function disableAction(Request $request, \Swift_Mailer $mailer)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $request->query->get('id')]);

        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }

        $announce->getIsActive() === false ? $announce->setIsActive(true) : $announce->setIsActive(false);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($announce);
        $entityManager->flush();

        $subject = $announce->getIsActive() === true ? "Votre annonce a été activé" : "votre annonce a été refusé";
        $body = $announce->getIsActive() === true ? "Bonjour, <br> Votre annonce a bien été activé" : "Bonjour, <br> votre annonce a malheureusement été refusé";
        $message = (new \Swift_Message($subject))
            ->setFrom('noreply@zikaloka.fr')
            ->setTo($this->getUser()->getEmail())
            ->setBody(
                $this->renderView($body),
                'text/html'
            );

        $mailer->send($message);

        return $this->redirectToRoute('list_announce');
    }

    //permet d'ajouter une annonce
    public function addAction(Request $request)
    {
        $announce = new Announce();
        $formAnnounce = $this->createForm(AnnounceType::class, $announce)
            ->add('save', SubmitType::class, ['label' => 'Envoyer']);

        $formAnnounce->handleRequest($request);


        if ($formAnnounce->isSubmitted() && $formAnnounce->isValid()) {

            $announce->setUser($this->getUser());
            $announce->setIsActive(null);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($announce);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('announce_part2', array('id' => $announce->getId())));
        }

        return $this->render(
            'Announce/add_announce.html.twig',
            [
                'formAnnounce' => $formAnnounce->createView(),
            ]
        );
    }


    //permet d'ajouter une photo a une annonce
    public function addPhotos(Request $request, $id)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['id' => $id]);

        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }

        $photos = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->findBy(['announce' => $id]);

        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo)
            ->add('save', SubmitType::class, ['label' => 'Ajouter']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //pour chaque photo, on génère un nom unique
            $fileName = md5(uniqid()) . '.' . $photo->getFile()->guessExtension();

            //pour chaque photo, on la déplace dans public/announces
            $photo->getFile()->move(
                $this->getParameter('photos_directory'),
                $fileName
            );

            //pour chaque photo, on enlève l'objet photo upload au départ, pour le remplacer par le nouvel objet avec le nouveau path
            $photo->setFile('/announces/' . $fileName);
            $photo->setAnnounce($announce);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($photo);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('announce_part2', array('id' => $id)));
        }
        return $this->render(
            'Announce/photos.html.twig',
            [
                'form' => $form->createView(),
                'photos' => $photos,
                'announce' => $announce
            ]
        );
    }

    //permet de supprimer une photo d'une annonce
    public function deletePhoto($id, $id_photo)
    {
        $photo = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->findOneBy(['announce' => $id, 'id' => $id_photo]);
        if (empty($photo)) {
            return $this->redirect($this->generateUrl('announce_part2', array('id' => $id)));
        }
        unlink($this->get('kernel')->getRootDir() . '/../public' . $photo->getFile());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($photo);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('announce_part2', array('id' => $id)));
    }

    //permet de supprimer une annonce
    public function deleteAction($id)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($announce);
        $entityManager->flush();

        return $this->redirectToRoute('list_announce');
    }

    //permet d'afficher toutes les annonce qui sont activé et qui ont des dates de disponibilités
    public function allListAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $query = $this->getDoctrine()->getManager()->createQueryBuilder()
                ->select('a')
                ->from('App:Announce', 'a')
                ->where('a.isActive = true AND a.user <> :user')
                ->setParameter('user', $this->getUser());

            $announces = $query->getQuery()->getResult();
            foreach ($announces as $announce) {
                $dispo = $this->getDoctrine()
                    ->getRepository(Disponibility::class)
                    ->findOneBy(['announce' => $announce->getId()]);
                if (empty($dispo)) {
                    unset($announces[array_search($announce, $announces)]);
                }

            }
        } else {
            $announces = $this->getDoctrine()->getRepository(Announce::class)
                ->findBy(['isActive' => true]);
            foreach ($announces as $announce) {
                $dispo = $this->getDoctrine()
                    ->getRepository(Disponibility::class)
                    ->findOneBy(['announce' => $announce->getId()]);
                if (empty($dispo)) {
                    unset($announces[array_search($announce, $announces)]);
                }
            }
        }


        return $this->render(
            'all_announces.html.twig',
            [
                'listAnnounce' => $announces
            ]
        );
    }

    //permet d'ajouter des dates de disponibilités
    public function addDisponibilities(Request $request, $id)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['id' => $id]);
        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }

        $disponibilities = $this->getDoctrine()
            ->getRepository(Disponibility::class)
            ->findBy(['announce' => $id]);

        $disponibility = new Disponibility();
        $form = $this->createForm(DisponibilityType::class, $disponibility)
            ->add('save', SubmitType::class, ['label' => 'Ajouter']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $disponibility->setAnnounce($announce);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($disponibility);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('announce_part3', array('id' => $id)));

        }

        return $this->render(
            'Announce/disponibilities.html.twig',
            [
                'form' => $form->createView(),
                'disponibilities' => $disponibilities
            ]
        );
    }

    //permet de supprimer une date de disponibilité
    public function deleteDisponibility(Request $request, $id, $id_disponibility)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $id]);
        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }
        $disponibility = $this->getDoctrine()
            ->getRepository(Disponibility::class)
            ->findOneBy(['id' => $id_disponibility]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($disponibility);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('announce_part3', array('id' => $id)));

    }

    //permet d'afficher les dates de disponibilités de l'annonce
    public function showAnnounceAction($id)
    {
        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['id' => $id]);

        if (empty($announce)) {
            return $this->redirectToRoute('list_announce');
        }

        return $this->render(
            'show_announce.html.twig',
            [
                'announce' => $announce,
                'user' => $announce->getUser(),
                'disponibility' => $announce->getDisponibilities()->getSnapshot()
            ]
        );
    }

    //permet de contacter le loueur
    public function contactRenterAction(Request $request, $id, \Swift_Mailer $mailer)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['id' => $id]);

        $user = $announce->getUser();

        $message = new Message();

        $formMessage = $this->createForm(MessageType::class, $message)
            ->add('save', SubmitType::class, ['label' => 'Envoyer']);

        $formMessage->handleRequest($request);
        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            try {
                $message->setSentDate(new \DateTime());
                $message->setUserIdSender($this->getUser());
                $message->setUserIdReceiver($user);


                $query = $this->getDoctrine()->getManager()->createQueryBuilder()
                    ->select('m')
                    ->from('App:Message', 'm')
                    ->where('m.userIdSender = :id_user OR m.userIdReceiver = :id_user')
                    ->setMaxResults(1)
                    ->setParameter('id_user', $this->getUser());
                $convers = $query->getQuery()->getResult();

                if (empty($convers)) {
                    $message->setConversation(uniqid());
                } else {
                    $message->setConversation($convers[0]);
                }


                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($message);
                $entityManager->flush();


                $email = $user->getEmail();
                $subject = $message->getSubject();
                $bodymessage = $message->getMessage();

                $validator = Validation::createValidator();

                $violationsSubject = $validator->validate($subject, array(
                    new NotBlank(),
                ));
                $violationsMessage = $validator->validate($bodymessage, array(
                    new NotBlank(),
                ));

                if (0 !== count($violationsSubject)) {
                    // there are errors, now you can show them
                    $this->addFlash(
                        'error',
                        'Veuillez renseigner le sujet du message'
                    );
                }
                if (0 !== count($violationsMessage)) {
                    // there are errors, now you can show them
                    $this->addFlash(
                        'error',
                        'Veuillez renseigner le corps du message'
                    );
                }
                if (count($violationsSubject) === 0 && count($violationsMessage) === 0) {
                    $body = "Bonjour " . $user->getFirstname() . "," . PHP_EOL . "Vous avez un nouveau message privé de la part de " . $this->getUser()->getFirstname() . " " . $this->getUser()->getLastname() . "." . PHP_EOL;
                    $mail = (new \Swift_Message("Vous avez un nouveau message privé sur Zikaloka"))
                        ->setFrom('noreply@zikaloka.fr')
                        ->setTo($email)
                        ->setBody(
                            $this->renderView(
                                'email.html.twig',
                                array('body' => $body)
                            ))
                        ->setContentType('text/plain');

                    if ($mailer->send($mail)) {
                        $this->addFlash('success', 'Message envoyé avec succès !');
                    } else {
                        $this->addFlash(
                            'error',
                            'Erreur d\'envoi, veuillez rééssayer'
                        );
                    }
                }

                return $this->redirect($this->generateUrl('contact_renter', array('id' => $announce->getId())));
            } catch (\Exception $e) {
                $this->addFlash('error', 'Un problème est survenue, veuillez rééssayer. '.$e->getMessage());

                return $this->redirect($this->generateUrl('contact_renter', array('id' => $announce->getId())));
            }
        }

        return $this->render(
            'contact_renter.html.twig',
            [
                'user' => $user,
                'announce' => $announce,
                'formMessage' => $formMessage->createView()
            ]
        );
    }

    //permet de reserver une annonce
    public function rentAction($id, Request $request, \Swift_Mailer $mailer)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $announce = $this->getDoctrine()
            ->getRepository(Announce::class)
            ->findOneBy(['id' => $id]);

        $renter = $this->getUser();

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('d')
            ->from('App:Disponibility', 'd')
            ->leftJoin('App:Rental', 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 'd.id = r.disponibility')
            ->where('IDENTITY(d.announce) = :id')
            ->andWhere('IDENTITY(r.disponibility) is NULL')
            ->orderBy('d.date', 'ASC')
            ->setParameter('id', $id);


        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Réserver',
                'attr' => ['class' => 'btn white_link']
            ))
            ->getForm();

        $allDispo = $qb->getQuery()->getResult();

        foreach ($allDispo as $dispo) {
            $form->add($dispo->getId(), CheckboxType::class, array(
                'label' => false,
                'required' => false,
                'attr' => ['id' => $dispo->getId()]
            ));
            $form->add($dispo->getDate()->format('dmY'), HiddenType::class, array(
                'attr' => ['id' => $dispo->getId(), 'value' => $dispo->getDate()->format('d/m/Y')]
            ));
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $resa = $form->getData();
            foreach ($allDispo as $dispo) {
                $bool[] = $resa[$dispo->getId()];
                $date[] = $resa[$dispo->getDate()->format('dmY')];

            }
            if (!isset($bool) || !isset($date)) {
                $this->addFlash(
                    'error',
                    'Erreur'
                );
                return $this->redirect($this->generateUrl('make_rental', array('id' => $id)));
            }

            foreach ($bool as $el) {
                $idelement = array_search($el, $bool);

                $iddate = $date[$idelement];
                if ($el) {
                    $datefinale = str_replace('/', '-', $iddate);
                    $dat = new \DateTime($datefinale);

                    $dispo = $this->getDoctrine()->getManager()->getRepository(Disponibility::class)->findOneBy(['date' => $dat, 'announce' => $announce]);

                    $rent = new Rental();
                    $rent->setAnnounce($announce);
                    $rent->setUser($this->getUser());
                    $rent->setDisponibility($dispo);
                    $rent->setChoiceDate($dispo->getDate());
                    $rent->setConfirmed(false);
                    $rent->setMark(0);
                    // tell Doctrine you want to (eventually) save the Product (no queries yet)

                    $verif = $this->getDoctrine()
                        ->getRepository(Rental::class)
                        ->findOneBy(['disponibility' => $dispo, 'announce' => $announce]);
                    if (!$verif) {

                        $this->getDoctrine()->getManager()->persist($rent);

                        // actually executes the queries (i.e. the INSERT query)
                        $this->getDoctrine()->getManager()->flush();
                        unset($bool[$idelement]);
                        unset($date[$idelement]);

                        $mail = (new \Swift_Message("Vous avez une demande de réservation en attente"))
                            ->setFrom('noreply@zikaloka.fr')
                            ->setTo($announce->getUser()->getEmail())
                            ->setBody('Votre annonce ' . $announce->getTitle() . ' a été réservée par ' . $this->getUser()->getEmail() . PHP_EOL . 'Rendez vous sur votre profil pour accepter ou décliner cette demande.');
                        $mail->setContentType("text/plain");
                        $mail->setCharset('utf-8');

                        $mailer->send($mail);
                        $this->addFlash(
                            'error',
                            'Votre demande de réservation a été envoyée au loueur'
                        );

                    } else {
                        $this->addFlash(
                            'error',
                            'Cette date est déjà réservée'
                        );

                    }

                }
            }

            return $this->redirect($this->generateUrl('make_rental', array('id' => $id)));
        }


        return $this->render(
            'make_rent.html.twig',
            [
                'allDispo' => $allDispo,
                'renter' => $renter,
                'announce' => $announce,
                'form' => $form->createView()
            ]
        );
    }

    public function showRentAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $rentals = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('r')
            ->from('App:Rental', 'r')
            ->where('r.user = :user AND r.choice_date >= :date')
            ->orderBy('r.choice_date', 'ASC')
            ->setParameter('user', $this->getUser())
            ->setParameter('date', new \DateTime('now'));
        $userRentals = $rentals->getQuery()->getResult();
        return $this->render(
            'show_rent_announces.html.twig',
            [
                'userRentals' => $userRentals,
            ]
        );

    }

    public function rentProgressAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $rentals = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('r')
            ->from('App:Announce', 'a')
            ->innerJoin('App:Rental', 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 'a.id = r.announce')
            ->where('a.user = :user')
            ->orderBy('r.choice_date', 'ASC')
            ->setParameter('user', $this->getUser());
        $userRentals = $rentals->getQuery()->getResult();
        return $this->render(
            'renter_show_rentals.html.twig',
            [
                'userRentals' => $userRentals,
            ]
        );
    }

    //permet de confirmer une reservation d'annonce
    public function rentConfirmAction($id, \Swift_Mailer $mailer)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $rental = $this->getDoctrine()->getManager()->getRepository(Rental::class)->findOneBy(['id' => $id]);
        $rental->setConfirmed(true);
        $this->getDoctrine()->getManager()->persist($rental);
        $this->getDoctrine()->getManager()->flush();

        $rentals = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('r')
            ->from('App:Announce', 'a')
            ->innerJoin('App:Rental', 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 'a.id = r.announce')
            ->where('a.user = :user')
            ->orderBy('r.choice_date', 'ASC')
            ->setParameter('user', $this->getUser());
        $userRentals = $rentals->getQuery()->getResult();

        $mail = (new \Swift_Message("Votre demande de réservation a été refusée"))
            ->setFrom('noreply@zikaloka.fr')
            ->setTo($rental->getUser()->getEmail())
            ->setBody('Votre demande de réservation pour la date ' . $rental->getChoiceDate()->format('d/m/Y') . ' sur l\'annonce ' . $rental->getAnnounce()->getTitle() . ' a été acceptée.' . PHP_EOL . 'Coordonnées du loueur : ' . PHP_EOL . 'Identité : ' . $rental->getUser()->getPrenom() . ' ' . $rental->getUser()->getNom() . PHP_EOL . 'Email : ' . $rental->getAnnounce()->getUser()->getEmail());
        $mail->setContentType("text/plain");
        $mail->setCharset('utf-8');

        $mailer->send($mail);
        return $this->render(
            'renter_show_rentals.html.twig',
            [
                'userRentals' => $userRentals,
            ]
        );
    }

    //permet d'annuler une reservation
    public function rentCancelAction($id, \Swift_Mailer $mailer)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $rental = $this->getDoctrine()->getManager()->getRepository(Rental::class)->findOneBy(['id' => $id]);
        $this->getDoctrine()->getManager()->remove($rental);
        $this->getDoctrine()->getManager()->flush();

        $rentals = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('r')
            ->from('App:Announce', 'a')
            ->innerJoin('App:Rental', 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 'a.id = r.announce')
            ->where('a.user = :user')
            ->orderBy('r.choice_date', 'ASC')
            ->setParameter('user', $this->getUser());

        $userRentals = $rentals->getQuery()->getResult();
        $mail = (new \Swift_Message("Votre demande de réservation a été refusée"))
            ->setFrom('noreply@zikaloka.fr')
            ->setTo($rental->getUser()->getEmail())
            ->setBody('Votre demande de réservation pour la date ' . $rental->getChoiceDate() . ' sur l\'annonce ' . $rental->getAnnounce()->getTitle() . ' a été refusée.');
        $mail->setContentType("text/plain");
        $mail->setCharset('utf-8');

        $mailer->send($mail);
        return $this->render(
            'renter_show_rentals.html.twig',
            [
                'userRentals' => $userRentals,
            ]
        );
    }
}
