<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class ContactController extends Controller
{
    //permet d'envoyer une message aux administrateurs
    public function contactAdminAction(Request $request, \Swift_Mailer $mailer)
    {
        $message = new Message();
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $formMessage = $this->createForm(MessageType::class, $message)
                ->add('email', EmailType::class, ['label' => 'Votre adresse email', 'mapped' => false])
                ->add('save', SubmitType::class, ['label' => 'Envoyer']);
        } else {
            $formMessage = $this->createForm(MessageType::class, $message)
                ->add('email', EmailType::class, ['label' => 'Votre adresse email', 'mapped' => false, 'attr' => array('readonly' => true, 'required' => true, 'value' => $this->getUser()->getEmail())])
                ->add('save', SubmitType::class, ['label' => 'Envoyer']);
        }
        $formMessage->handleRequest($request);
        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            try {

                if (empty($formMessage->get('email')->getData())) {
                    $email = $this->getUser()->getEmail();
                } else {
                    $email = $formMessage->get('email')->getData();
                }

                $subject = $message->getSubject();
                $bodymessage = $message->getMessage();
                if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                    $introBody = 'De : <' . $email . '>';
                } else {
                    $introBody = 'De : ' . $this->getUser()->getFirstname() . ' ' . $this->getUser()->getLastname() . ' <' . $email . '>';
                }


                $mail = (new \Swift_Message($subject))
                    ->setFrom($email)
                    ->setTo('zikaloka.contact@gmail.com')
                    ->setBody($introBody . PHP_EOL . $bodymessage);
                $mail->setContentType("text/plain");
                $mail->setCharset('utf-8');

                if ($mailer->send($mail)) {
                    $this->addFlash('success', 'Message envoyé avec succès !');
                } else {
                    $this->addFlash(
                        'error',
                        'Erreur d\'envoi, veuillez rééssayer'
                    );
                }

                return $this->redirect($this->generateUrl('contact_admin'));
            } catch (\Exception $e) {
                $this->addFlash('error', 'Un problème est survenue, veuillez rééssayer.' . $e->getMessage());
                return $this->redirect($this->generateUrl('contact_admin'));
            }
        }

        return $this->render(
            'contact_admin_form.html.twig',
            [
                'formMessage' => $formMessage->createView()
            ]
        );
    }

}