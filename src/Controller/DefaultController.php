<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    //affiche le page d'accueil
    public function indexAction()
    {
        return new Response($this->twig->render(
            'base.html.twig'
        ));
    }

    //affiche la page des FAQ
    public function faqAction()
    {
        return new Response($this->twig->render(
            'faq.html.twig'
        ));
    }
}
