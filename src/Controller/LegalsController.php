<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class LegalsController
{
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    //affiche la page des mentions légales
    public function indexAction()
    {
        return new Response($this->twig->render(
            'legals.html.twig'
        ));
    }
}
