<?php
/**
 * Created by PhpStorm.
 * User: loic
 * Date: 05/07/18
 * Time: 20:58
 */

namespace App\Controller;

use App\Entity\Rental;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MarkController extends Controller
{
    //affiche la page notation d'une annonce
    public function showAction($id)
    {
        $rent = $this->getDoctrine()
            ->getRepository(Rental::class)
            ->findOneBy(['announce' => $id, 'user' => $this->getUser()->getId()]);

        if (empty($rent)) {
            return $this->redirectToRoute('list_announce');
        }

        return $this->render('show_mark.html.twig',
            [
                'rent' => $rent,
            ]);
    }

    //permet de noté une annonce
    public function addAction($id, $note)
    {
        $rental = $this->getDoctrine()
            ->getRepository(Rental::class)
            ->findOneBy(['id' => $id]);

        if ($rental->getMark() === 0) {
            $rental->setMark($note);
            $this->getDoctrine()->getManager()->persist($rental);
            $this->getDoctrine()->getManager()->flush();

            $rentals = $this->getDoctrine()
                ->getRepository(Rental::class)
                ->findBy(['announce' => $rental->getAnnounce()->getId()]);
            if (!empty($rentals)) {
                $note = 0;
                $i = 0;
                foreach ($rentals as $rent) {
                    $note += $rent->getMark();
                    $i++;
                }

                $rental->getAnnounce()->setMark($note / $i);
                $this->getDoctrine()->getManager()->persist($rental->getAnnounce());
                $this->getDoctrine()->getManager()->flush();
            }

        } else {
            $this->addFlash('danger', 'vous avez deja noté cette annonce.');
        }

        return $this->redirectToRoute('mark_show', ['id' => $rental->getAnnounce()->getId()]);
    }
}