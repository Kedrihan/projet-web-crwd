<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 06/06/2018
 * Time: 10:59
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\MessageType;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends Controller
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    //permet d'afficher les messages d'une conversation et d'y repondre
    public function displayAction(Request $request, \Swift_Mailer $mailer, $id)
    {

        if ($id === null) {
            return $this->redirectToRoute('messages');
        }

        $form = $this->createForm(MessageType::class)
            ->remove('subject')
            ->add('submit', SubmitType::class, ['label' => "Envoyer"]);

        $form->handleRequest($request);


        $query = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('m')
            ->from('App:Message', 'm')
            ->where('m.conversation = :id')
            ->orderBy('m.sent_date')
            ->setParameter('id', $id);
        $messages = $query->getQuery()->getResult();


        if (empty($messages)) {
            return $this->redirectToRoute('messages');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            $message->setSubject("");
            $message->setConversation($id);
            $message->setSentDate(new \DateTime('now'));
            $message->setUserIdSender($this->getUser());

            foreach ($messages as $msg) {
                if ($msg->getUserIdSender() === $this->getUser()) {

                    $user = $this->getDoctrine()
                        ->getRepository(User::class)
                        ->findOneBy(['id' => $msg->getUserIdReceiver()]);

                    $message->setUserIdReceiver($user);
                } else if ($msg->getUserIdReceiver() === $this->getUser()) {

                    $user = $this->getDoctrine()
                        ->getRepository(User::class)
                        ->findOneBy(['id' => $msg->getUserIdSender()]);

                    $message->setUserIdReceiver($user);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();


            $message = (new \Swift_Message('Vous avez un reçu un  nouveau message sur Zikaloka.fr'))
                ->setFrom('noreply@zikaloka.fr')
                ->setTo($this->getUser()->getEmail())
                ->setBody(
                    'Bonjour, <br> vous avez reçu un nouveau message sur le site Zikaloka.fr',
                    'text/html'
                );

            $mailer->send($message);

            return $this->redirectToRoute('message', ['id' => $id]);
        }

        return $this->render('conversation.html.twig',
            [
                'form' => $form->createView(),
                'messages' => $messages,
            ]
        );
    }
}