<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 16/05/2018
 * Time: 09:53
 */

namespace App\Controller;

use App\Entity\User;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MessagesController extends Controller
{
    //permet d'afficher les la liste des conversations
    public function listAction()
    {

        $query = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('m')
            ->from('App:Message', 'm')
            ->where('m.userIdSender = :id_user OR m.userIdReceiver = :id_user')
            ->groupBy('m.conversation')
            ->orderBy('m.sent_date', 'DESC')
            ->setParameter(':id_user', $this->getUser());

        $messages = $query->getQuery()->getResult();

        /*for ($i = 0; $i < count($messages); $i++) {
            $messages[$i]['id_user'] = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['id' => $messages[$i]->getUserIdReceiver()]));
        }*/


        return $this->render(
            'messages.html.twig',
            [
                'listMessages' => $messages,
            ]
        );
    }
}
