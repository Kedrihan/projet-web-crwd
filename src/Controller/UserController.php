<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 04/07/2018
 * Time: 06:49
 */

namespace App\Controller;

use App\Entity\Rental;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    //permet de supprimer un utilisateur
    public function deleteAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByUsername($this->getUser()->getUsername());

        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();


        $query = $this->getDoctrine()->getManager()->createQueryBuilder();

        $query->select('m')->from('App:Message', 'm')->where('m.userIdSender = :user')->orWhere('m.userIdReceiver = :user')->setParameter(':user', $user->getId());
        $result = $query->getQuery()->getResult();

        if (!empty($result)){
            foreach ($result as $conversation){
                $this->getDoctrine()->getManager()->remove($conversation);
                $this->getDoctrine()->getManager()->flush();
            }
        }



        if (!empty($result)){
            foreach ($result as $rental){
                $this->getDoctrine()->getManager()->remove($rental);
                $this->getDoctrine()->getManager()->flush();
            }
        }
dump($result);
        exit;


        $userManager->deleteUser($user);
        return $this->redirectToRoute('index');
    }
}