<?php

namespace App\DataFixtures;

use App\Entity\Family;
use App\Entity\Instrument;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FamilyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $data = ['Instruments à cordes', 'Instruments à vent (bois)', 'Instruments à vent (cuivres)', 'Percussions'];
        $datas = [
            ['Violon', 'Alto', 'Violoncelle', 'Contrebasse', 'Guitare', 'Lyre', 'Viode gambe', 'Banjo', 'Contrebassine', 'Harpe', 'Luth', 'Mandoline', 'Gouroumi', 'Clavecin', 'Epinette', 'Piano'],
            ['Flute à bec', 'Flute traversière', 'Hautbois', 'Basson', 'Clarinette', 'Saxophone'],
            ['Trompette', 'Cor d\'harmonie', 'Trombone', 'Tuba'],
            ['Xylophone', 'Vibraphone', 'Marimba', 'Timbales', 'Caisse claire', 'Grosse Caisse', 'Toms', 'Cymbales', 'Tambour', 'Tambourin', 'Triangle', 'Castagnettes', 'Claves', 'Gong', 'Caixa', 'Tambotim']
        ];

        for ($i = 0; $i < 4; $i++) {
            $family = new Family();
            $family->setName($data[$i]);
            $manager->persist($family);
            for ($y = 0; $y < count($datas[$i]); $y++) {
                $instrument = new Instrument();
                $instrument->setName($datas[$i][$y]);
                $instrument->setDefaultPhoto('test');
                $instrument->setFamilyId($family);
                $manager->persist($instrument);
            }
        }
        $manager->flush();

    }
}
