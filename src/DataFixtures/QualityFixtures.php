<?php
/**
 * Created by PhpStorm.
 * User: boyer
 * Date: 02/03/2018
 * Time: 15:37
 */

namespace App\DataFixtures;

use App\Entity\Quality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class QualityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $data = ['Très Bon', 'Bon', 'Moyen'];

        for ($i = 0; $i < 3; $i++) {
            $quality = new Quality();
            $quality->setName($data[($i)]);
            $manager->persist($quality);
        }
        $manager->flush();
    }
}
