<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Announce
 *
 * @ORM\Table(name="announce")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\AnnounceRepository")
 */
class Announce
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */

    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=false)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="brandname", type="string", length=255, nullable=false)
     */
    private $brandname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="purchase_date", type="date", length=255, nullable=false)
     */
    private $purchase_date;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="mark", type="integer", precision=10, scale=0, nullable=true)
     */
    private $mark;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime", nullable=false)
     */
    private $modification_date;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Photo", mappedBy="announce" , cascade={"remove"}, fetch="EAGER")
     */
    private $photos;

    /**
     * @var \App\Entity\Instrument
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Instrument", inversedBy="announces", cascade={"persist"})
     */
    private $instrument;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="announces", fetch="EAGER")
     */
    private $user;


    /**
     * @var \App\Entity\Quality
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Quality", inversedBy="announces")
     */
    private $quality;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Disponibility", mappedBy="announce", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $disponibilities;

    /**
     * Announce constructor.
     */
    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->disponibilities = new ArrayCollection();
        $this->modification_date = new \DateTime();
        $this->creation_Date = new \DateTime();
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getBrandname(): ?string
    {
        return $this->brandname;
    }

    /**
     * @param string $brandname
     */
    public function setBrandname(string $brandname): void
    {
        $this->brandname = $brandname;
    }

    /**
     * @return \DateTime
     */
    public function getPurchaseDate(): ?\DateTime
    {
        return $this->purchase_date;
    }

    /**
     * @param \DateTime $purchase_date
     */
    public function setPurchaseDate(\DateTime $purchase_date): void
    {
        $this->purchase_date = $purchase_date;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getMark(): ?int
    {
        return $this->mark;
    }

    /**
     * @param int $mark
     */
    public function setMark(int $mark): void
    {
        $this->mark = $mark;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creation_Date;
    }

    /**
     *
     * @ORM\PrePersist()
     */

    public function setCreationDate(): void
    {
        $this->creation_Date = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modification_date;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setModificationDate(): void
    {
        $this->modification_date = new \DateTime();
    }

    /**
     * @return bool
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    /**
     * @param Photo $photo
     */
    public function removePhoto(Photo $photo): void
    {
        $this->photos->removeElement($photo);
        // set the owning side to null
        //$photo->setAnnounce(null);
    }

    /**
     * @param Photo $photo
     */
    public function addPhoto(Photo $photo): void
    {
        if ($this->photos->contains($photo)) {
            return;
        }
        $this->photos[] = $photo;
        // set the *owning* side!
        $photo->setAnnounce($this);
    }

    /**
     * @return Instrument
     */
    public function getInstrument(): ?Instrument
    {
        return $this->instrument;
    }

    /**
     * @param Instrument $instrument
     */
    public function setInstrument(Instrument $instrument): void
    {
        $this->instrument = $instrument;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        $user->addAnnounce($this);
    }

    /**
     * @return Quality
     */
    public function getQuality(): ?Quality
    {
        return $this->quality;
    }

    /**
     * @param Quality $quality
     */
    public function setQuality(Quality $quality): void
    {
        $this->quality = $quality;
    }

    /**
     * @return ArrayCollection
     */
    public function getDisponibilities(): Collection
    {
        return $this->disponibilities;
    }

    /**
     * @param Disponibility $disponibility
     */
    public function removeDisponibility(Disponibility $disponibility): void
    {
        $this->disponibilities->removeElement($disponibility);
        // set the owning side to null
        $disponibility->setAnnounce(null);
    }

    /**
     * @param Disponibility $disponibility
     */
    public function addDisponibility(Disponibility $disponibility): void
    {
        if ($this->disponibilities->contains($disponibility)) {
            return;
        }
        $this->disponibilities[] = $disponibility;
        // set the *owning* side!
        $disponibility->setAnnounce($this);
    }
}
