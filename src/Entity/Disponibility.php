<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Disponibility
 *
 * @ORM\Table(name="disponibility")
 * @ORM\Entity
 */
class Disponibility
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\Announce
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Announce", inversedBy="disponibilities", cascade={"persist"})
     */
    private $announce;

    /**
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Announce
     */
    public function getAnnounce(): Announce
    {
        return $this->announce;
    }

    /**
     * @param Announce $announce
     */
    public function setAnnounce(Announce $announce): void
    {
        $this->announce = $announce;
    }


}
