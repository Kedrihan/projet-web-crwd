<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Instrument
 *
 * @ORM\Table(name="instrument")
 * @ORM\Entity
 */
class Instrument
{

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="default_photo", type="string", length=255, nullable=false)
     */
    private $default_photo;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\Family
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Family")
     */
    private $familyId;

    /**
     * @var ArrayCollection
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="App\Entity\Announce", mappedBy="instrument" , cascade={"persist", "remove"})
     */
    private $announces;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDefaultPhoto(): ?string
    {
        return $this->default_photo;
    }

    /**
     * @param string $default_photo
     */
    public function setDefaultPhoto(string $default_photo): void
    {
        $this->default_photo = $default_photo;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Family
     */
    public function getFamilyId(): Family
    {
        return $this->familyId;
    }

    /**
     * @param Family $familyId
     */
    public function setFamilyId(Family $familyId): void
    {
        $this->familyId = $familyId;
    }
}
