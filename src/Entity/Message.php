<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity
 */
class Message
{
    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_date", type="datetime", nullable=false)
     */
    private $sent_date;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $userIdSender;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $userIdReceiver;

    /**
     * @var int
     *
     * @ORM\Column(name="conversation", type="string", nullable=false)
     */
    private $conversation;

    /**
     * @return int
     */
    public function getConversation(): string
    {
        return $this->conversation;
    }

    /**
     * @param int $conversation
     */
    public function setConversation(string $conversation): void
    {
        $this->conversation = $conversation;
    }


    /**
     * @return string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return \DateTime
     */
    public function getSentDate(): ?\DateTime
    {
        return $this->sent_date;
    }

    /**
     * @param \DateTime $sent_date
     */
    public function setSentDate(\DateTime $sent_date): void
    {
        $this->sent_date = $sent_date;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUserIdSender(): ?User
    {
        return $this->userIdSender;
    }

    /**
     * @param User $userIdSender
     */
    public function setUserIdSender(User $userIdSender): void
    {
        $this->userIdSender = $userIdSender;
    }

    /**
     * @return User
     */
    public function getUserIdReceiver(): ?User
    {
        return $this->userIdReceiver;
    }

    /**
     * @param User $userIdReceiver
     */
    public function setUserIdReceiver(User $userIdReceiver): void
    {
        $this->userIdReceiver = $userIdReceiver;
    }


}
