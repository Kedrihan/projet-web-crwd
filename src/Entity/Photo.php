<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Photo
 *
 * @ORM\Table(name="photos")
 * @ORM\Entity
 */
class Photo
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\Image(
     *     detectCorrupted=true
     *     )
     *
     * @Assert\NotBlank(message="Please, upload a correct picture.")
     */
    private $file;
    /**
     * @return mixed
     */

    /**
     * @var \App\Entity\Announce
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Announce", inversedBy="photos", cascade={"persist"} )
     */
    private $announce;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @return Announce
     */
    public function getAnnounce(): Announce
    {
        return $this->announce;
    }

    /**
     * @param Announce $announce
     */
    public function setAnnounce(Announce $announce): void
    {
        $this->announce = $announce;
    }


}
