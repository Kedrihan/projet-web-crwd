<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Quality
 *
 * @ORM\Table(name="quality")
 * @ORM\Entity
 */
class Quality
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ArrayCollection
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="App\Entity\Announce", mappedBy="quality" , cascade={"persist", "remove"})
     */
    private $announces;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnnounces(): ?ArrayCollection
    {
        return $this->announces;
    }

    /**
     * @param ArrayCollection $announces
     */
    public function setAnnounces(ArrayCollection $announces): void
    {
        $this->announces = $announces;
    }


}
