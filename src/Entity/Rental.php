<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rental
 *
 * @ORM\Table(name="rental")
 * @ORM\Entity
 */
class Rental
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="choice_date", type="date", length=255, nullable=false)
     */
    private $choice_date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmed", type="boolean", length=255, nullable=false)
     */
    private $confirmed;

    /**
     * @var integer
     *
     * @ORM\Column(name="mark", type="integer", length=255, nullable=false)
     */
    private $mark;

    /**
     * @var \App\Entity\Announce
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Announce", cascade={"remove"})
     */
    private $announce;

    /**
     * @var \App\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"remove"})
     */
    private $user;

    /**
     * @var \App\Entity\Disponibility
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Disponibility", cascade={"remove"})
     */
    private $disponibility;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return \DateTime
     */
    public function getChoiceDate(): \DateTime
    {
        return $this->choice_date;
    }

    /**
     * @param \DateTime $choice_date
     */
    public function setChoiceDate(\DateTime $choice_date): void
    {
        $this->choice_date = $choice_date;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    /**
     * @param bool $confirmed
     */
    public function setConfirmed(bool $confirmed): void
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return int
     */
    public function getMark(): int
    {
        return $this->mark;
    }

    /**
     * @param int $mark
     */
    public function setMark(int $mark): void
    {
        $this->mark = $mark;
    }

    /**
     * @return Announce
     */
    public function getAnnounce(): Announce
    {
        return $this->announce;
    }

    /**
     * @param Announce $announce
     */
    public function setAnnounce(Announce $announce): void
    {
        $this->announce = $announce;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Disponibility
     */
    public function getDisponibility(): Disponibility
    {
        return $this->disponibility;
    }

    /**
     * @param Disponibility $disponibility
     */
    public function setDisponibility(Disponibility $disponibility): void
    {
        $this->disponibility = $disponibility;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


}
