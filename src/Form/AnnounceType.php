<?php

namespace App\Form;

use App\Entity\Announce;
use App\Entity\Instrument;
use App\Entity\Quality;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;

class AnnounceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description', TextareaType::class)
            ->add('reference')
            ->add('brandname')
            ->add('purchase_date', DateType::class, array(
                'widget' => 'single_text',
                'constraints' => [new LessThan(new \DateTime('today'))]

            ))
            ->add('price')
            ->add('instrument', EntityType::class, array(
                'class' => Instrument::class,
                'choice_label' => 'name',

            ))
            ->add('quality', EntityType::class, array(
                'class' => Quality::class,
                'choice_label' => 'name',

            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Announce::class,
        ]);
    }
}
