<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 10/02/2018
 * Time: 10:29
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThan;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('gender', ChoiceType::class, ['choices' => ['Mr' => 'Monsieur', 'Mme' => 'Madame']]);
        $builder->add('lastname', TextType::class);
        $builder->add('firstname', TextType::class);
        $builder->add('city', TextType::class);
        $builder->add('phone', TextType::class);
        $builder->add('birthdate', DateType::class, array(
            'widget' => 'single_text',
            'constraints' => [new LessThan(new \DateTime('-18 years'))]
        ));

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    // For Symfony 2.x

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}