<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180703184349 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE announce CHANGE instrument_id instrument_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE quality_id quality_id INT DEFAULT NULL, CHANGE mark mark INT DEFAULT NULL, CHANGE is_active is_active TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE disponibility CHANGE announce_id announce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE instrument CHANGE family_id_id family_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message CHANGE user_id_sender_id user_id_sender_id INT DEFAULT NULL, CHANGE user_id_receiver_id user_id_receiver_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photos CHANGE announce_id announce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE rental CHANGE announce_id announce_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE disponibility_id disponibility_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE salt salt VARCHAR(255) DEFAULT NULL, CHANGE last_login last_login DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT NULL, CHANGE password_requested_at password_requested_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE announce CHANGE instrument_id instrument_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE quality_id quality_id INT DEFAULT NULL, CHANGE mark mark INT DEFAULT NULL, CHANGE is_active is_active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE disponibility CHANGE announce_id announce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE instrument CHANGE family_id_id family_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message CHANGE user_id_sender_id user_id_sender_id INT DEFAULT NULL, CHANGE user_id_receiver_id user_id_receiver_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photos CHANGE announce_id announce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE rental CHANGE announce_id announce_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL, CHANGE disponibility_id disponibility_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE salt salt VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE last_login last_login DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE password_requested_at password_requested_at DATETIME DEFAULT \'NULL\'');
    }
}
