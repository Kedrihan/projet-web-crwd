<?php
# src/Vendor/Project/tests/units/HelloWorld.php

// La classe de test a son propre namespace :
// Le namespace de la classe à tester + "tests\units"
namespace App\Tests\Units\Controller;

use atoum;

/*
 * Classe de test pour Vendor\Project\HelloWorld
 *
 * Remarquez qu'elle porte le même nom que la classe à tester
 * et qu'elle dérive de la classe atoum
 */
class DefaultController extends atoum
{
    /*
     * Cette méthode est dédiée à la méthode getHiAtoum()
     */
    public function testGetHiAtoum()
    {

        $twig = new \mock\Twig_Environment(new \mock\Twig_LoaderInterface());
        $this->calling($twig)->render = new \mock\Symfony\Component\HttpFoundation\Response();
        $this
            // création d'une nouvelle instance de la classe à tester
            ->given($this->newTestedInstance($twig));
            // nous testons que la méthode getHiAtoum retourne bien
            // une chaîne de caractère...
        $this->assert()
            ->object($this->testedInstance->indexAction())
            ->isInstanceOf('Symfony\Component\HttpFoundation\Response')

        ;
    }
}
